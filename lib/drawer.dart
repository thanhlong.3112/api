import 'package:api_flutter/screen/check_conection_screen.dart';
import 'package:api_flutter/screen/fibonacci.dart';
import 'package:api_flutter/screen/movie_list_screen.dart';
import 'package:flutter/material.dart';

class DrawerMenu extends StatelessWidget {
  const DrawerMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text('Drawer Header'),
          ),
          ListTile(
            title: const Text('Movie List'),
            onTap: () {
              Navigator.pushReplacementNamed(context, MoviesScreen.routeName);
            },
          ),
          ListTile(
            title: const Text('Check Connection'),
            onTap: () {
              Navigator.pushReplacementNamed(
                  context, CheckConnectionScreen.routeName);
            },
          ),
          ListTile(
            title: const Text('Check Fibonacci Screen'),
            onTap: () {
              Navigator.pushReplacementNamed(
                  context, CheckFibonacciScreen.routeName);
            },
          ),
        ],
      ),
    );
  }
}
