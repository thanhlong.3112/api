// import 'package:connectivity_plus/connectivity_plus.dart';
// import 'package:flutter/material.dart';

// import 'drawer.dart';

// class CheckConnectionScreen extends StatefulWidget {
//   static const routeName = '/check';
//   const CheckConnectionScreen({Key? key}) : super(key: key);

//   @override
//   _CheckConnectionScreenState createState() => _CheckConnectionScreenState();
// }

// class _CheckConnectionScreenState extends State<CheckConnectionScreen> {
//   CheckConnectionScreen result =
//       ConnectivityResult.none as CheckConnectionScreen;
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       drawer: DrawerMenu(),
//       appBar: AppBar(
//         title: Text('Check Connection Screen'),
//       ),
//       body: Center(
//           child: ElevatedButton(
//               child: Text('Check Connection'),
//               onPressed: () async {
//                 result = (await Connectivity().checkConnectivity())
//                     as CheckConnectionScreen;
//                 if (result == ConnectivityResult.wifi) {
//                   ScaffoldMessenger.of(context)
//                       .showSnackBar(SnackBar(content: Text('wifi connect')));
//                 } else if (result == ConnectivityResult.mobile) {
//                   ScaffoldMessenger.of(context)
//                       .showSnackBar(SnackBar(content: Text('Mobile connect')));
//                 } else {
//                   ScaffoldMessenger.of(context)
//                       .showSnackBar(SnackBar(content: Text('no connect')));
//                 }
//               })),
//     );
//   }
// }
