import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'movie.dart';

class MovieProvider extends ChangeNotifier {
  final String url =
      'https://api.themoviedb.org/3/movie/popular?api_key=65302d9fd57dda3ea2ba86f370ab6b7f&language=en-US&page=1';
  Future<MovieModel> fetchMovie() async {
    print('fetchMovie');
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return MovieModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception(response.statusCode);
    }
  }
}
