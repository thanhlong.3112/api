import 'package:api_flutter/drawer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../movie.dart';
import '../provider.dart';
import 'detail_movie_list_screen.dart';

class MoviesScreen extends StatefulWidget {
  static const routeName = '/movies_screen';
  const MoviesScreen({Key? key}) : super(key: key);

  @override
  State<MoviesScreen> createState() => _MoviesScreenState();
}

class _MoviesScreenState extends State<MoviesScreen> {
  late Future<MovieModel> _movieFuture;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _movieFuture =
        Provider.of<MovieProvider>(context, listen: false).fetchMovie();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: DrawerMenu(),
        appBar: AppBar(
          title: const Text('Movies List'),
        ),
        body: FutureBuilder<MovieModel>(
          future: _movieFuture,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final results = snapshot.data!.results;
              return GridView.builder(
                padding: const EdgeInsets.all(15),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 20,
                  crossAxisSpacing: 20,
                  childAspectRatio: 2 / 3,
                ),
                itemCount: results?.length,
                itemBuilder: (context, index) {
                  return ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: GridTile(
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => MovieDetailScreen(
                                movie: results![index],
                              ),
                            ),
                          );
                        },
                        child: CachedNetworkImage(
                          imageUrl:
                              'https://image.tmdb.org/t/p/w500${results?[index].posterPath}',
                          progressIndicatorBuilder:
                              (context, url, downloadProgress) => Center(
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: CircularProgressIndicator(
                                  value: downloadProgress.progress),
                            ),
                          ),
                          imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          errorWidget: (context, url, error) =>
                              const Icon(Icons.error),
                        ),
                      ),
                      footer: GridTileBar(
                        backgroundColor: Colors.black87,
                        title: Text(
                          results?[index].originalTitle ?? '--',
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  );
                },
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.hasError}');
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ));
  }
}
