import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../movie.dart';

class MovieDetailScreen extends StatelessWidget {
  static const routeName = '/movie_detail';
  const MovieDetailScreen({Key? key, required this.movie}) : super(key: key);
  final Result movie;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            buildImage((imageProvider) => BoxDecoration(
                  color: Colors.black87,
                  image: DecorationImage(
                      colorFilter: ColorFilter.mode(
                          Colors.black.withOpacity(0.3), BlendMode.dstATop),
                      image: imageProvider,
                      fit: BoxFit.cover),
                )),
            buildDetail(),
            ...buildIcon(context),
          ],
        ),
      ),
    );
  }

  Container buildDetail() {
    return Container(
      padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
      height: 400,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 3,
            child: buildImage((imageProvider) => BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image:
                      DecorationImage(image: imageProvider, fit: BoxFit.cover),
                )),
          ),
          Expanded(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  CircleAvatar(
                    radius: 30,
                    backgroundColor: const Color.fromRGBO(3, 37, 65, 0.5),
                    child: Text(
                      ('${((movie.voteAverage ?? 0) * 10).toStringAsFixed(0)}%')
                          .toString(),
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                  const SizedBox(height: 8),
                  CircleAvatar(
                    radius: 30,
                    backgroundColor: const Color.fromRGBO(3, 37, 65, 0.5),
                    child: IconButton(
                      icon: const Icon(
                        Icons.list,
                        color: Colors.white,
                      ),
                      onPressed: () {},
                    ),
                  ),
                  const SizedBox(height: 8),
                  CircleAvatar(
                    radius: 30,
                    backgroundColor: const Color.fromRGBO(3, 37, 65, 0.5),
                    child: IconButton(
                      icon: const Icon(
                        Icons.favorite,
                        color: Colors.white,
                      ),
                      onPressed: () {},
                    ),
                  ),
                  const SizedBox(height: 8),
                  CircleAvatar(
                    radius: 30,
                    backgroundColor: const Color.fromRGBO(3, 37, 65, 0.5),
                    child: IconButton(
                      icon: const Icon(
                        Icons.bookmark,
                        color: Colors.white,
                      ),
                      onPressed: () {},
                    ),
                  ),
                  const SizedBox(height: 8),
                  CircleAvatar(
                    radius: 30,
                    backgroundColor: const Color.fromRGBO(3, 37, 65, 0.5),
                    child: IconButton(
                      icon: const Icon(
                        Icons.star,
                        color: Colors.white,
                      ),
                      onPressed: () {},
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }

  CachedNetworkImage buildImage(
      Function(ImageProvider imageProvider) decoration) {
    return CachedNetworkImage(
      imageUrl: 'https://image.tmdb.org/t/p/w500${movie.posterPath}',
      progressIndicatorBuilder: (context, url, downloadProgress) => Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: CircularProgressIndicator(value: downloadProgress.progress),
        ),
      ),
      imageBuilder: (context, imageProvider) => Container(
        decoration: decoration(imageProvider),
      ),
      errorWidget: (context, url, error) => const Icon(Icons.error),
    );
  }

  List<Widget> buildIcon(BuildContext context) => [
        Positioned(
          top: 25,
          left: 5,
          child: IconButton(
            color: Colors.white,
            onPressed: () => Navigator.pop(context),
            icon: const Icon(Icons.arrow_back),
          ),
        ),
        const SizedBox(height: 20),
        Builder(builder: (context) {
          final diviceSize = MediaQuery.of(context).size;
          return Positioned(
            top: 400,
            height: diviceSize.height - 420,
            width: diviceSize.width,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      movie.originalTitle ?? '--',
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 34,
                      ),
                    ),
                    Text(
                      '${movie.releaseDate} (US)',
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                        color: Colors.white70,
                        fontSize: 18,
                      ),
                    ),
                    const SizedBox(height: 20),
                    const Text(
                      'Overview',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                      ),
                    ),
                    const SizedBox(height: 10),
                    Text(
                      movie.overview ?? '--',
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                        color: Colors.white70,
                        fontSize: 18,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        })
      ];
}
