import 'dart:isolate';
import 'dart:math';

import 'package:api_flutter/drawer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CheckFibonacciScreen extends StatefulWidget {
  static const routeName = '/check-fibonacci';
  const CheckFibonacciScreen({Key? key}) : super(key: key);

  @override
  _CheckFibonacciScreenState createState() => _CheckFibonacciScreenState();
}

class _CheckFibonacciScreenState extends State<CheckFibonacciScreen> {
  final _numberControler = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerMenu(),
      appBar: AppBar(
        title: Text('Check Fibonacci Screen'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Form(
            child: Column(
              children: [
                TextFormField(
                  keyboardType: TextInputType.number,
                  controller: _numberControler,
                  decoration: const InputDecoration(
                    labelText: 'Number',
                  ),
                  onChanged: (value) {
                    _numberControler.text = value;
                  },
                ),
                const SizedBox(
                  height: 15,
                ),
                ElevatedButton(
                    onPressed: () {
                      // isCompute();
                      var i = int.parse(_numberControler.text);
                      isFibonacci(i)
                          ? _showSnackBar('$i is fibonacci')
                          : _showSnackBar('$i is not fibonacci');
                    },
                    child: Text('CHECK'))
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showSnackBar(text) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(text)));
  }

  bool isPerfectSquare(int x) {
    int s = sqrt(x).toInt();
    return (s * s == x);
  }

  bool isFibonacci(int n) {
    return isPerfectSquare(5 * n * n + 4) || isPerfectSquare(5 * n * n - 4);
  }

  // void isCompute() async {
  //   var results = await compute(calculate, int.parse(_numberControler.text));
  //   _showSnackBar('$results is a Fibonacci');
  // }

  // static int calculate(int num) {
  //   bool isPerfectSquare(int x) {
  //     int s = sqrt(x).toInt();
  //     return (s * s == x);
  //   }

  //   bool isFibonacci(int n) {
  //     return isPerfectSquare(5 * n * n + 4) || isPerfectSquare(5 * n * n - 4);
  //   }

  //   if (isFibonacci(num) == true) {
  //     return num;
  //   } else {

  //   }
  // }

}
