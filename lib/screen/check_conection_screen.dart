import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';

import '../drawer.dart';

class CheckConnectionScreen extends StatefulWidget {
  static const routeName = '/check';
  const CheckConnectionScreen({Key? key}) : super(key: key);

  @override
  _CheckConnectionScreenState createState() => _CheckConnectionScreenState();
}

class _CheckConnectionScreenState extends State<CheckConnectionScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerMenu(),
      appBar: AppBar(
        title: Text('Check Connection Screen'),
      ),
      body: Center(
        child: StreamBuilder(
          stream: Connectivity().onConnectivityChanged,
          builder: (context, AsyncSnapshot<ConnectivityResult> snapshot) {
            return ElevatedButton(
                onPressed: () {
                  snapshot.data == ConnectivityResult.mobile ||
                          snapshot.data == ConnectivityResult.wifi
                      ? _showDialog('Internet access', 'You are connected')
                      : _showDialog('No Internet', 'You are not connected ');
                },
                child: Text('Check Connection'));
          },
        ),
      ),
    );
  }

  void _showDialog(title, text) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(title),
            content: Text(text),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('OK'))
            ],
          );
        });
  }

  void _showSnackBar(text) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(text)));
  }
}
