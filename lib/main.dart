import 'package:api_flutter/provider.dart';
import 'package:api_flutter/screen/check_conection_screen.dart';
import 'package:api_flutter/screen/fibonacci.dart';
import 'package:api_flutter/screen/movie_list_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<MovieProvider>(
      create: (context) => MovieProvider(),
      child: MaterialApp(
        routes: {
          MoviesScreen.routeName: (context) => const MoviesScreen(),
          CheckConnectionScreen.routeName: (context) =>
              const CheckConnectionScreen(),
          CheckFibonacciScreen.routeName: (context) => CheckFibonacciScreen(),
        },
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: CheckFibonacciScreen(),
      ),
    );
  }
}
